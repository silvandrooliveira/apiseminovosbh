# API SeminovosBH

A API SeminovosBH foi desenvolvida com o framework PHP **Laravel Lumem**, e a utilização da biblioteca PHP cliente HTTP **Guzzle** e o **PHP Simple DOM Parser**.

Para executar a aplicação em ambiente local, inicialmente é necessário acessar o diretório raiz **apiseminovosbh** via terminal Linux, e posteriormente digitar o seguinte comando:

```php -S localhost:8000 -t public```

Após a execução do comando no terminal, poderá ser acessado o navegador, e ser realizada a busca dos veículos a partir de um dos dois caminhos a seguir:

```https://localhost:8000/listarVeiculos?parametro1=valor1&parametro2=valor2``` - pesquisa realizada com filtros da página inicial do site SeminovosBH, sendo possível passar os seguintes parâmetros e valores a URL:

| Parâmetro |             Descrição            |     Exemplo    |
|:---------:|:--------------------------------:|:--------------:|
|   marca   |         Marca do Veículo         |  marca=Renault |
|   modelo  |         Modelo do Veículo        | modelo=Sandero |
|   anoDe   | Ano inicial de fabricação/modelo |   anoDe=2012   |
|   anoAte  |  Ano final de fabricação/modelo  |   anoAte=2019  |
|  precoDe  |    Preço mais baixo do veículo   |  precoDe=20000 |
|  precoAte |    Preço mais alto do veículo    | precoAte=30000 |

<br>
```https://localhost:8000/listarDados/id``` - pesquisa específica por algum veículo pelo id do mesmo, no qual retorna os mesmos dados da pesquisa geral, porém apenas do veículo pesquisado.

Em ambas pesquisas, é retornado um JSON com os seguintes dados conforme exemplo abaixo:

```
[
    {
        "id":"2688761",
        "link":"https:\/\/www.seminovos.com.br\/renault-sandero-2012-2013--2688761",
        "titulo":"Renault Sandero",
        "valor":"R$ 22.000,00",
        "versao":"VERS\u00c3O: Expression Hi-Flex 1.6 8V 5p","ano":"2012-2013",
        "quilometragem":"108.000 km",
        "cambio":"Manual"
    }
]
```

Ambas consultas utilizam a passagem dos parâmetros via método HTTP POST, de modo similar como é realizado pelo site SeminovosBH.