<?php

/**
 * Arquivo com as rotas para listar veiculos e listar dados de um veiculo
 * @author Silvandro Oliveira <silvandro_oliveira@hotmail.com>
 * @package routes
 * @version 1.0.0
 * @since Integrado ao projeto desde 26/01/2020
 */

$router->get('/listarVeiculos', 'VeiculoGeralController@index');
$router->get('/listarDados/{id}', 'VeiculoEspecificoController@index');
