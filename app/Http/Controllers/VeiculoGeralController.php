<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\VeiculoTrait;

/**
 * Controller da aplicacao para buscar dados do Seminovosbh
 * @author Silvandro Oliveira <silvandro_oliveira@hotmail.com>
 * @package app/Http
 * @subpackage Controllers
 * @version 1.0.0
 * @since Integrado ao projeto desde 26/01/2020
 */
class VeiculoGeralController extends Controller
{
    use VeiculoTrait;
    
    /**
     * Metodo index da aplicacao que retorna dados dos veiculos pesquisados a
     * partir dos parametros da URL
     * @access public
     * @param Request $request Parametros da URL
     * @return array Array com os dados dos veiculos pesquisados
     */
    public function index(Request $request)
    {
        return $this->getDados($request);
    }
    
    /**
     * Metodo que busca veiculos com os parametros passados via URL a partir de
     * elementos contidos no codigo HTML da pagina do site
     * @access private
     * @param Request $parametros Parametros passados pela URL
     * @return array Array com dados do veiculo pesquisado
     */
    public function getDados($parametros)
    {
        $url = $this->montarURL($parametros);
        $html = $this->getHtml($url);
        
        $arrayDados = null;
        
        foreach ($html->find('.card-content') as $value) {
            
            $veiculo = $value->children;
            $href = $veiculo[0]->attr['href'];
            $inicioId = strpos($href, '--') + 2;
            $finalId = strpos($href, '?');
            
            $arrayDados[] = [
                'id' => substr($href, $inicioId, $finalId - $inicioId),
                'link' => $this->getUrlDefault() . substr($href, 0, $finalId),
                'titulo' => trim($veiculo[0]->find('h2')[0]->plaintext),
                'valor' => trim($veiculo[0]->find('.card-price')[0]->plaintext),
                'versao' => trim($veiculo[1]->
                        find('.card-subtitle')[0]->plaintext),
                'ano' => trim($veiculo[1]->
                        find('li[title=Ano de fabricação]')[0]->plaintext),
                'quilometragem' => trim($veiculo[1]->
                        find('li[title=Kilometragem atual]')[0]->plaintext),
                'cambio' => trim($veiculo[1]->
                        find('li[title=Tipo de câmbio]')[0]->plaintext)
            ];
        }
        
        return $arrayDados;
    }
    
    /**
     * Metodo que monta a URL para pesquisa de veiculos a partir dos parametros
     * passados pela URL
     * @access public
     * @param Request $request Parametros passados pela URL
     * @return string URL para pesquisa dos veiculos
     */
    public function montarUrl($request)
    {
        $url = $this->getUrlDefault();
        $url .= '/carro';
        
        if (!empty($request->query())) { 
            if ($request->has('marca')) {
                $url .= '/' . $request->get('marca');
            }
            if ($request->has('marca') && $request->has('modelo')) {
                $url .= '/' . $request->get('modelo');
            }
            if ($request->has('anoDe') || $request->has('anoAte')) {
                $url .= '/ano-';
                if ($request->has('anoDe')) {
                    $url .= $request->get('anoDe');
                }
                $url .= '-';
                if ($request->has('anoAte')) {
                    $url .= $request->get('anoAte');
                }
            }
            if ($request->has('precoDe') || $request->has('precoAte')) {
                $url .= '/preco-';
                if ($request->has('precoDe')) {
                    $url .= $request->get('precoDe');
                }
                $url .= '-';
                if ($request->has('precoAte')) {
                    $url .= $request->get('precoAte');
                }
            }
        }
        
        return $url;
    }
}
