<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\VeiculoTrait;

/**
 * Controller para geracao de dados de um veiculo em especifico
 * @author Silvandro Oliveira <silvandro_oliveira@hotmail.com>
 * @package app/Http
 * @subpackage Controllers
 * @version 1.0.0
 * @since Integrado ao projeto desde 26/01/2020
 */
class VeiculoEspecificoController extends Controller
{
    use VeiculoTrait;
    
    /**
     * Metodo index da aplicacao que retorna dados de um veiculo especifico
     * @access public
     * @param Request $request Parametros da url
     * @param int $id Id do veiculo pesquisado
     * @return JSON String JSON com dados do veiculo pesquisado
     */
    public function index(Request $request, $id)
    {
        return $this->getDados($id);
    }

    /**
     * Metodo que busca os dados de determinado veiculo a partir de elementos
     * contidos no codigo HTML da pagina do site
     * @access private
     * @param int $id Id do veiculo pesquisado
     * @return array Array com dados do veiculo pesquisado
     */
    public function getDados($id)
    {
        $url = $this->montarUrl($id);
        $html = $this->getHtml($url);
        $veiculo = $html->find('.item-info');
        
        return [
            'id' => $id,
            'link' => $url,
            'titulo' => trim($veiculo[0]->find('h1')[0]->plaintext),
            'valor' => trim($veiculo[0]->find('span')[0]->plaintext),
            'versao' => trim($veiculo[0]->find('.desc')[0]->plaintext),
            'ano' => trim($veiculo[0]->
                    find('span[title=Ano/modelo]')[0]->plaintext),
            'quilometragem' => trim($veiculo[0]->
                    find('span[title=Kilometragem do veículo]')[0]->plaintext),
            'cambio' => trim($veiculo[0]->
                    find('span[title=Tipo de transmissão]')[0]->plaintext)
        ];
    }

    /**
     * Metodo que monta a URL para pesquisa dos dados do veiculo
     * @access public
     * @param int $parametro Id do registro do veiculo
     * @return string Url do veiculo a ser pesquisado
     */
    public function montarUrl($parametro)
    {
        return $this->getUrlDefault() . '/' . $parametro;
    }
}
