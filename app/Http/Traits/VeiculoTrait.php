<?php

namespace App\Http\Traits;

use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Trait contendo metodos em comum nas aplicacoes que buscam veiculos pelos
 * parametros da URL e dados de um determinado veiculo
 * @author Silvandro Oliveira <silvandro_oliveira@hotmail.com>
 * @package app/Http
 * @subpackage Controllers
 * @version 1.0.0
 * @since Integrado ao projeto desde 26/01/2020
 */
trait VeiculoTrait
{
    /**
     * Metodo que os dados do HTML da pagina
     * @access public
     * @param string $url URL da pagina a ser pesquisada
     * @return \simplehtmldom_1_5\simple_html_dom Objeto 
     * PHP Simple HTML DOM Parser
     */
    public function getHtml($url)
    {
        $client = new Client();
        $html = $client->post($url);
        
        return HtmlDomParser::str_get_html($html->getBody());
    }
    
    /**
     * Metodo que retorna a URL default da pagina pesquisada
     * @access public
     * @return string URL default da pagina pesquisada
     */
    public function getUrlDefault()
    {
        return 'https://www.seminovos.com.br';
    }
}
